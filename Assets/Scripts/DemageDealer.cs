﻿using UnityEngine;

public class DemageDealer : MonoBehaviour
{
    [SerializeField] int demage = 100;

    public int GetDemage() { return demage; }

    public void Hit()
    {
        Destroy(gameObject);
    }
}
