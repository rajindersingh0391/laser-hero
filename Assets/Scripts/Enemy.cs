﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour
{
    [Header("General")]
    [SerializeField] int health = 100;
    float shotCounter;
    [SerializeField] float minTimeBetweenShots = 0.2f;
    [SerializeField] float maxTimeBetweenShots = 3f;
    [SerializeField] int enemyDeathPoints = 100;
    [SerializeField] GameObject sparklesVFX;  // for stars bursting out when enemy dies

    [Header("Projectile")]
    [SerializeField] GameObject enemyLaserPrefab;
    [SerializeField] float projectileSpeed = 10f;  

    [Header("Audio")]
    [SerializeField] AudioClip dieSound;
    [SerializeField] AudioClip shootSound;  //laser shoot sound
    [SerializeField] [Range(0, 1)] float deathSoundVolume = 0.75f;
    [SerializeField] [Range(0, 1)] float shootSoundVolume = 0.75f;
    
    private void Start()
    {
        shotCounter = UnityEngine.Random.Range(minTimeBetweenShots, maxTimeBetweenShots);
    }

    private void Update()
    {
        CountDownAndShoot();
    }

    private void CountDownAndShoot()
    {
        shotCounter -= Time.deltaTime;
        if (shotCounter <= 0f)
        {
            Fire();
            shotCounter = UnityEngine.Random.Range(minTimeBetweenShots, maxTimeBetweenShots);
        }
    }

    private void Fire() //fire the laser
    {
        GameObject enemyLaser = Instantiate(enemyLaserPrefab, transform.position, Quaternion.identity) as GameObject;
        enemyLaser.GetComponent<Rigidbody2D>().velocity = new Vector2(0, -projectileSpeed);
        AudioSource.PlayClipAtPoint(shootSound, Camera.main.transform.position, shootSoundVolume);  // sound when enemy shoots a laser
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        Debug.Log("Enemy Collided with: " + collision.gameObject);
        DemageDealer demageDealer = collision.gameObject.GetComponent<DemageDealer>();  // getting demage dealer of object which bumped into enemy
        if(!demageDealer)
        {
            Debug.Log("DemageDealer component does not exist");
            return;
        }

        ProcessHit(demageDealer);
    }

    private void ProcessHit(DemageDealer demageDealer)
    {      
        health -= demageDealer.GetDemage();
        demageDealer.Hit(); // Destroy the laser

        if (health <= 0)
        {
            Die();
        }
    }

    private void Die()
    {
        //Debug.Log("Enemy died");
        FindObjectOfType<GameSession>().AddToScore(enemyDeathPoints);
        AudioSource.PlayClipAtPoint(dieSound, Camera.main.transform.position, deathSoundVolume); // Play a sound when enemy dies
        Destroy(gameObject);
        TriggerSparklesVFX();       
    }

    private void TriggerSparklesVFX()
    {
        GameObject sparkles = Instantiate(sparklesVFX, transform.position, transform.rotation);
        Destroy(sparkles, 1f);
    }

}
