﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyPathing : MonoBehaviour
{
    // configuration params
    WaveConfig waveConfig;  

    // variables
    int wayPointIndex = 0;
    List<Transform> waypoints;

    // Start is called before the first frame update
    void Start()
    {
        Debug.Log("In start method of enemy pathing");
        waypoints = waveConfig.GetWayPoints();
        transform.position = waypoints[wayPointIndex].transform.position;   // set the starting position for enemy (which is first way point position)
    }

    // Update is called once per frame
    void Update()
    {
        Move();
    }

    public void SetWaveConfig(WaveConfig waveConfig)
    {
        this.waveConfig = waveConfig;
    }

    private void Move()
    {
        if (waypoints.Count >=0 && wayPointIndex <= waypoints.Count - 1)
        {
            var targetPos = waypoints[wayPointIndex].transform.position;   // setting enemy's target position
            var movementThisFrame = waveConfig.GetMoveSpeed() * Time.deltaTime;  // setting the speed with which enemy will reach its target Postion
            transform.position = Vector2.MoveTowards(transform.position, targetPos, movementThisFrame);  // starts to move the enemy

            if (transform.position == targetPos) // if enemy has reached the waypoint increase the waypoint index
            {
                wayPointIndex++;
            }
        }
        else
        {
            Destroy(gameObject);
        }
    }
}
