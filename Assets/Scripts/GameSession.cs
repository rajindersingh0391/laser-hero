﻿using UnityEngine;

public class GameSession : MonoBehaviour
{
    int score = 0;

    private void Awake()
    {
        SetUpSingleton();
    }

    private void SetUpSingleton()
    {
        if (FindObjectsOfType(GetType()).Length > 1)
        {
            // Game object gets detroyed at end of lifecycle, so there could be a chance that there is more than one object for a split second.
            //That is the reason we make the game object inactive so its stops working and then gets destroyed afterwards. https://docs.unity3d.com/Manual/ExecutionOrder.html
            gameObject.SetActive(false);
            Destroy(gameObject); // When game jumps to another scene, it destroys its Game Status object
        }
        else
        {
            DontDestroyOnLoad(gameObject); // When game jumpts to another scene, it sees it only has one object which is retained
        }
    }

    public int GetScore()
    {
        return score;
    }

    public void AddToScore(int scoreVal)
    {
        score += scoreVal;
    }

    public void ResetGame()
    {
        Destroy(gameObject);
    }
}
