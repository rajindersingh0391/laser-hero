﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;

public class Level : MonoBehaviour
{
    [SerializeField] float delayInSeconds = 2f;

    public void LoadStartMenu()
    {
        SceneManager.LoadScene(0);
    }

    public void LoadGame()
    {
        SceneManager.LoadScene("Game");

        if (FindObjectsOfType<GameSession>().Length > 1)
        {
            FindObjectOfType<GameSession>().ResetGame();    // Destroys the game session and score as well
        }
    }

    public void LoadGameOver()
    {
        SceneManager.LoadScene("GameOver");
        //StartCoroutine(WaitAndLoad());
    }

    public void QuitGame()
    {
        Application.Quit();
    }

    IEnumerator WaitAndLoad()
    {
        Debug.Log("In WaitAndLoad Coroutine");
        yield return new WaitForSeconds(delayInSeconds);
        Debug.Log("After WaitForSeconds");
        SceneManager.LoadScene("GameOver");
    }
}
