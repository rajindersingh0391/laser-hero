﻿using TMPro;
using UnityEngine;

public class PlayerHealth : MonoBehaviour
{
    TextMeshProUGUI playerHealthText;
    Player player;

    // Start is called before the first frame update
    void Start()
    {
        player = FindObjectOfType<Player>();
        playerHealthText = GetComponent<TextMeshProUGUI>();
    }

    // Update is called once per frame
    void Update()
    {
        //Debug.Log("Player health: " + player.GetPlayerHealth().ToString());
        playerHealthText.text = player.GetPlayerHealth().ToString(); // Setting player health to Score Text object
    }
}
